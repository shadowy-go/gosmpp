package gosmpp

import "gitlab.com/shadowy-go/gosmpp/Exception"

type OutbindEventListener interface {
	HandleOutbind(outbind *OutbindEvent) *Exception.Exception
}
