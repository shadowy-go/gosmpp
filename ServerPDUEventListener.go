package gosmpp

import "gitlab.com/shadowy-go/gosmpp/Exception"

type ServerPDUEventListener interface {
	HandleEvent(event *ServerPDUEvent) *Exception.Exception
}
